interface CreateIssue {
	projectId: number
	title: string
	description: string
	labels?: string[]
}

const pushToGitlab = (order: CreateIssue) => {
	const endpoint = `https://gitlab.com/api/v4/projects/${order.projectId}/issues`
	const options = {
		method: 'post' as const,
		contentType: 'application/json' as const,
		headers: {
			'PRIVATE-TOKEN': '***PRIVATE-TOKEN***',
		},
		payload: JSON.stringify({
			id: order.projectId,
			title: order.title,
			description: order.description,
			labels: order.labels?.join(','),
		}),
	}
	return UrlFetchApp.fetch(endpoint, options)
}

const test = () => {
	pushToGitlab({
		projectId: Number('***PROJECT-ID***'),
		title: 'test',
		description: 'testest',
		labels: ['お問い合わせ'],
	})
}

const flat = (res: string | (string | string[])[]): string => {
	if ('string' === typeof res) return res
	return res.map(p => flat(p)).join('\n')
}

const onFormSubmit = (e: GoogleAppsScript.Events.FormsOnFormSubmit) => {
	const order: CreateIssue = {
		projectId: Number('***PROJECT-ID***'),
		title: '',
		description: '',
		labels: ['お問い合わせ'],
	}
	const itemResponses = e.response.getItemResponses()
	let ref = ''
	let title = ''
	let content = ''
	for (const formData of itemResponses) {
		const itemTitle = formData.getItem().getTitle()
		const response = flat(formData.getResponse())
		switch (itemTitle) {
			case 'リンク元URL':
				ref = response
				break
			case '質問のタイトル':
				title = response
				break
			case '本文':
				content = response
				break
			default:
				break
		}
	}
	order.title = '【お問い合わせ】' + title
	const lines: string[] = []
	if (ref) lines.push('リンク元URL: ' + ref)
	if (title) lines.push('質問のタイトル: ' + title)
	if (content) {
		if (lines.length) lines.push('')
		lines.push('本文:', content.replace(/(\r?\n)/g, '  $1'))
	}
	order.description = lines.map(b => b + (b && '  ') + '\n').join('')
	pushToGitlab(order)
}
