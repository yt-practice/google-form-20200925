const { join } = require('path')
const { remove, mkdir, readFile, writeFile } = require('fs-extra')
const ts2gas = require('ts2gas')
require('dotenv/config')

remove(join(__dirname, '..', 'dist'))
	.catch(x => {
		console.error(x)
		process.exit(1)
	})
	.then(async () => {
		await mkdir(join(__dirname, '..', 'dist'))
		let c = ts2gas(await readFile(join(__dirname, '../src/index.ts'), 'utf-8'))
		const { GITLAB_ACCESS_TOKEN: token, GITLAB_PROJECT_ID: pId } = process.env
		c = c.replace(/\*\*\*PRIVATE-TOKEN\*\*\*/giu, token)
		c = c.replace(/Number\('\*\*\*PROJECT-ID\*\*\*'\)/giu, pId)
		return writeFile(join(__dirname, '../dist/index.js'), c)
	})
